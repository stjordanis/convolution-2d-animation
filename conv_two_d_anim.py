import os
import numpy as np
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt

animation_capable = False
try:
    import lodgepole.animation_tools as at
    animation_capable = True
except Exception:
    print("Unable to import lodgepole (video rendering tool)")
    print("    To be able to render videos first install it.")
    print("    Using the command line:")
    print()
    print("git clone https://gitlab.com/brohrer/lodgepole.git")
    print("python3 -m pip install -e lodgepole")
    print()
    print("    You'll also need to make sure you have FFMPEG installed.")
    print("    https://ffmpeg.org/")
    print()


plt.switch_backend("agg")

frames_dir = "frames"
os.makedirs(frames_dir, exist_ok=True)

fig_width = 16
fig_height = 9
frame_width = 3.2
frame_height = 3.2
img_spacing = .5
dpi = 100
small_frame_width = 5
bg_color = "xkcd:off white"
# bg_color = "ivory"
small_border_color = "xkcd:deep sky blue"
border_color = "midnightblue"
text_color = "midnightblue"
text_size = 20
pos_neg_cmap = LinearSegmentedColormap.from_list(
    "pos_neg",
    [(0, "red"), (.5, "black"), (1, "white")])
kernel_cmap = pos_neg_cmap
image_cmap = pos_neg_cmap
result_cmap = pos_neg_cmap
shadow = .4
lowlight = .5
highlight = 1
final_pause = 15  # frames


def render(image, kernel, name=None, slowdown=4):
    render_frames(image, kernel, slowdown)
    if animation_capable:
        render_video(name=name)


def render_frames(image, kernel, slowdown):
    # Clear out the frames directory
    for filename in os.listdir(frames_dir):
        if filename[-4:] == ".png":
            os.remove(os.path.join(frames_dir, filename))

    i_frame = 0
    n_image_rows, n_image_cols = image.shape
    n_kernel_rows, n_kernel_cols = kernel.shape
    n_result_rows = n_image_rows - n_kernel_rows + 1
    n_result_cols = n_image_cols - n_kernel_cols + 1
    results = np.zeros((n_result_rows, n_result_cols))
    for i_result_col in range(n_result_cols):
        for i_result_row in range(n_result_rows):
            render_frame(
                image,
                kernel,
                i_result_row,
                i_result_col,
                results)

            for _ in range(int(slowdown)):
                framename = os.path.join(frames_dir, f"f{10000 + i_frame}.png")
                plt.savefig(framename, dpi=dpi)
                i_frame += 1
            plt.close()

    render_frame(
        image,
        kernel,
        n_result_rows - 1,
        n_result_cols - 1,
        results)
    for _ in range(int(final_pause)):
        framename = os.path.join(frames_dir, f"f{10000 + i_frame}.png")
        plt.savefig(framename, dpi=dpi)
        i_frame += 1
    plt.close()


def render_frame(image, kernel, i_result_row, i_result_col, results):
    n_image_rows, n_image_cols = image.shape
    n_kernel_rows, n_kernel_cols = kernel.shape
    n_result_rows, n_result_cols = results.shape
    image_width = (
        frame_width * n_image_cols / np.maximum(n_image_rows, n_image_cols))
    image_height = (
        frame_height * n_image_rows / np.maximum(n_image_rows, n_image_cols))

    reversed_kernel = np.rot90(kernel, k=2, axes=(0, 1))
    patch = image[
        i_result_row: i_result_row + n_kernel_rows,
        i_result_col: i_result_col + n_kernel_cols]
    product = reversed_kernel * patch
    dot_product = np.sum(product)
    results[i_result_row, i_result_col] = dot_product

    fig = plt.figure(figsize=(fig_width, fig_height))
    ax_fig = fig.add_axes((0, 0, 1, 1), facecolor=bg_color)
    blank(ax_fig)
    ax_fig.set_xlim(0, fig_width)
    ax_fig.set_ylim(0, fig_height)

    # Create the first figure with the reversed kernel
    left = fig_width / 2 - 2 * frame_width - 1.5 * img_spacing
    bottom = (fig_height - frame_height) / 2
    center = left + frame_width / 2
    text_height = bottom + frame_height * 1.15
    ax_kernel_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    blank(ax_kernel_frame)
    ax_kernel_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "reversed kernel")

    pixel_width = image_width / n_image_cols
    pixel_height = image_height / n_image_rows
    kernel_left = (
        left + (frame_width - image_width) / 2 +
        pixel_width * i_result_col)
    kernel_bottom = (
        bottom - (frame_height - image_height) / 2 +
        frame_height - pixel_height * (n_kernel_rows + i_result_row))
    kernel_width = pixel_width * n_kernel_cols
    kernel_height = pixel_height * n_kernel_rows
    ax_kernel = fig.add_axes((
        kernel_left / fig_width,
        kernel_bottom / fig_height,
        kernel_width / fig_width,
        kernel_height / fig_height))
    ax_kernel.imshow(reversed_kernel, cmap=kernel_cmap, vmin=-1, vmax=1)
    blank(ax_kernel, small_frame_width, color=small_border_color)

    # Create the second figure with the image
    left = fig_width / 2 - frame_width - .5 * img_spacing
    center = left + frame_width / 2
    ax_img_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    ax_img_frame.imshow(image, cmap=image_cmap, vmin=-1, vmax=1)
    blank(ax_img_frame)
    ax_img_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "image")

    image_left = left + (frame_width - image_width) / 2
    image_bottom = bottom +  (frame_height - image_height) / 2
    ax_img = fig.add_axes((
        image_left / fig_width,
        image_bottom / fig_height,
        image_width / fig_width,
        image_height / fig_height))
    ax_img.imshow(image, cmap=image_cmap, vmin=-1, vmax=1)
    blank(ax_img)

    kernel_left = image_left + pixel_width * i_result_col
    ax_img_kernel = fig.add_axes((
        kernel_left / fig_width,
        kernel_bottom / fig_height,
        kernel_width / fig_width,
        kernel_height / fig_height))
    blank(ax_img_kernel, small_frame_width, color=small_border_color)
    ax_img_kernel.set_facecolor("none")

    # Create the third figure with the product of the image and the kernel
    left = fig_width / 2 + .5 * img_spacing
    center = left + frame_width / 2
    ax_prod_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    ax_prod_frame.imshow(image * shadow, cmap=image_cmap, vmin=-1, vmax=1)
    blank(ax_prod_frame)
    ax_prod_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "product")

    image_left = left + (frame_width - image_width) / 2
    image_bottom = bottom +  (frame_height - image_height) / 2
    ax_prod = fig.add_axes((
        image_left / fig_width,
        image_bottom / fig_height,
        image_width / fig_width,
        image_height / fig_height))
    ax_prod.imshow(image * shadow, cmap=image_cmap, vmin=-1, vmax=1)
    blank(ax_prod)

    kernel_left = image_left + pixel_width * i_result_col
    ax_prod_kernel = fig.add_axes((
        kernel_left / fig_width,
        kernel_bottom / fig_height,
        kernel_width / fig_width,
        kernel_height / fig_height))
    ax_prod_kernel.imshow(
        product * highlight, cmap=result_cmap, vmin=-1, vmax=1)
    blank(ax_prod_kernel, small_frame_width, color=small_border_color)

    # Create the fourth figure with the convolution results
    left = fig_width / 2 + frame_width + 1.5 * img_spacing
    center = left + frame_width / 2
    ax_result_frame = fig.add_axes((
        left / fig_width,
        bottom / fig_height,
        frame_width / fig_width,
        frame_height / fig_height))
    blank(ax_result_frame)
    ax_result_frame.set_facecolor(bg_color)
    add_title(ax_fig, center, text_height, "convolution")

    result_height = n_result_rows * pixel_height
    result_width = n_result_cols * pixel_width
    result_left = left + (frame_width - result_width) / 2
    result_bottom = bottom + (frame_height - result_height) / 2
    result_top = result_bottom + result_height
    ax_result = fig.add_axes((
        result_left / fig_width,
        result_bottom / fig_height,
        result_width / fig_width,
        result_height / fig_height))
    ax_result.imshow(results * lowlight, cmap=result_cmap, vmin=-1, vmax=1)
    blank(ax_result)

    result_new_left = result_left + pixel_width * i_result_col
    result_new_bottom = result_top - pixel_height * (i_result_row + 1)
    ax_result_new = fig.add_axes((
        result_new_left / fig_width,
        result_new_bottom / fig_height,
        pixel_width / fig_width,
        pixel_height / fig_height))
    blank(ax_result_new, small_frame_width / 2, color=small_border_color)
    ax_result_new.set_facecolor("none")


def blank(ax, borderwidth=1, color=border_color):
    ax.spines["top"].set_color(color)
    ax.spines["bottom"].set_color(color)
    ax.spines["right"].set_color(color)
    ax.spines["left"].set_color(color)
    ax.spines["top"].set_linewidth(borderwidth)
    ax.spines["bottom"].set_linewidth(borderwidth)
    ax.spines["right"].set_linewidth(borderwidth)
    ax.spines["left"].set_linewidth(borderwidth)
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)


def add_title(ax, x, y, text):
    ax.text(
        x,
        y,
        text,
        fontsize=text_size,
        color=text_color,
        horizontalalignment="center",
        verticalalignment="center",
    )


def render_video(name=None):
    if name is None:
        filename = "convolution_2D.mp4"
    else:
        filename = f"{name}.mp4"

    output_dir = "."
    at.render_movie(
        filename=filename,
        frame_dirname=frames_dir,
        output_dirname=output_dir)
    at.convert_to_gif(filename=filename, dirname=output_dir)


if __name__ == "__main__":
    image = np.array([
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ])
    kernel = np.array([
        [1, .5, 0],
        [.5, 0, -.5],
        [0, -.5, -1]
    ])
    render(image, kernel)
