# Animations of convolution in two dimensions (images)

This project lets you programmatically create your own videos of a two dimensional convolution operation.

![An example of the convolution animation](example.gif)

### Install

    git clone git@gitlab.com:brohrer/convolution-2d-animation.git

### Dependencies

    git clone https://gitlab.com/brohrer/lodgepole.git
    python3 -m pip install -e lodgepole

Also, install [FFMPEG](https://ffmpeg.org/)

### Test

    cd convolution-2d-animation
    python3 conv_two_d_anim.py

### Use

    from conv_two_d_anim import render
    render(image, kernel)

where `image` and ``kernel`` are 2D NumPy arrays.

To make a one-image summary of a convolution:

    from conv_two_d import render
    render(image, kernel)

![example.png](example.png)

### Gallery

![gallery_1.gif](gallery_1.gif)

![gallery_2.gif](gallery_2.gif)

![gallery_3.gif](gallery_3.gif)

![gallery_4.gif](gallery_4.gif)

![gallery_1.png](gallery_1.png)

![gallery_2.png](gallery_2.png)
